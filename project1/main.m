//
//  main.m
//  project1
//
//  Created by José Germán Mansilla Gutiérrez on 6/10/15.
//  Copyright (c) 2015 Germy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
