//
//  AppDelegate.h
//  project1
//
//  Created by José Germán Mansilla Gutiérrez on 6/10/15.
//  Copyright (c) 2015 Germy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

